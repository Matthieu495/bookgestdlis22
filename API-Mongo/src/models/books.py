from typing import Optional
from beanie import Document, Link
from pydantic import BaseModel

from models.keywords import Keyword
from models.topics import Topic


class Book(Document):
    title: str
    author: str
    resume: Optional[str]
    price: float
    image: Optional[str]
    keywords: list[Link[Keyword]] = []
    topics: list[Link[Topic]] = []
    stock: int

    class Settings:
        use_revision = True


class UpdateBook(BaseModel):
    title: Optional[str]
    author: Optional[str]
    resume: Optional[str]
    price: Optional[float]
    image: Optional[str]
    keywords: list[Link[Keyword]]
    topics: list[Link[Topic]]
    stock: Optional[int]
