from beanie import Document
from pydantic import BaseModel


class Register(BaseModel):
    email : str
    password : str
    lastname:str
    firstname:str
    password_confirmation:str
