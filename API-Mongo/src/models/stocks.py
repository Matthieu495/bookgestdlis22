from typing import Optional

from beanie import Document


class Stock(Document):
    level: Optional[int]
    security: Optional[int]
