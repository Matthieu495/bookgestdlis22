from datetime import datetime
from beanie import Document, PydanticObjectId, Indexed


class Order(Document):
    id_book: PydanticObjectId
    id_customer: PydanticObjectId
    quantity: float  # TODO a limiter à 10 après la virgule
    date: datetime = datetime.now()
    class Settings:
        use_revision = True
