import os
from typing import Annotated

from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from fastapi import Request, HTTPException, Depends
from starlette import status

from internal.auth_requests import call_auth
from models.customers import Customer


class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)
    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            current_user = self.verify_jwt(credentials.credentials)
            if not current_user:
                raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            current_user['token'] = credentials.credentials
            return current_user
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, jwtoken: str):
        try:
            [response, response_content] = call_auth(method='get', path='user', token_user=jwtoken)
            if response.status_code != 200:
                raise HTTPException(status_code=response.status_code, detail=response_content)
        except HTTPException as e:
            response_content = None
            raise e
        return response_content


def is_admin(current_user: Annotated[Customer,Depends(JWTBearer())]):

    if current_user["role"] == 'user':
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not an admin"
        )
    return True
