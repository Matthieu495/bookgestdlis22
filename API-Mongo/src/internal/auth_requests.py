import os

from fastapi import requests, HTTPException



from requests import JSONDecodeError


def call_auth(method: str, path: str, token_user: str = None, json: dict = None):
    request_method = getattr(requests, method)

    if token_user is not None:
        headers = {'Authorization': 'Bearer ' + token_user}
    else:
        headers = None

    if request_method is not None:
        response = request_method(os.environ['AUTH_HOST'] + '/' + path, headers=headers, json=json)
        try:
            response_content = response.json()
        except JSONDecodeError:
            response_content = response.text
        return [response, response_content]
    else:
        raise HTTPException(status_code=500, detail="Undefined http method")
