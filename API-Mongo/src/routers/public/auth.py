from typing import Annotated

from fastapi import APIRouter, Depends

from internal.auth import JWTBearer
from models.login import Login
from models.register import Register
from services import customers_service

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)

@router.post("/login")
async def login(login: Login):
    return customers_service.login(login)

@router.post("/register")
async def register(register: Register):
    return customers_service.register(register)

@router.post("/logout")
async def logout(token: Annotated[str, Depends(JWTBearer())]):
    return customers_service.logout(token)