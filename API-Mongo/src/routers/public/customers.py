from fastapi import APIRouter

from services import customers_service

router = APIRouter(
    prefix="/customers",
    tags=["customers"],
)


@router.get("/")
async def get_user(token: str):
    return customers_service.get_user(token)

@router.delete("/")
async def delete_user(token: str):
    return customers_service.delete_user(token)
@router.put("/password")
async def modify_password(token: str, password: str):
    return customers_service.modify_password(token, password)

@router.get("/email")
async def get_email(token: str):
    return customers_service.get_email(token)

@router.get("/lastname")
async def get_last_name(token: str):
    return customers_service.get_last_name(token)

@router.get("/firstname")
async def get_first_name(token: str):

    return customers_service.get_first_name(token)

@router.get("/role")
async def get_role(token: str):
    return customers_service.get_role(token)






