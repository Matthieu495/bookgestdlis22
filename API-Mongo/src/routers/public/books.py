from typing import List

from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue, mongo_to_vue_list
from models.books import Book
from services import books_service
from vues.book import BookVue
from vues.keyword import KeywordVue
from vues.order import OrderVue
from vues.topic import TopicVue

router = APIRouter(
    prefix="/books",
    tags=["books"],
)


@router.get("/", response_model=List[BookVue], description="Get all books")
async def get_books() -> List[BookVue]:
    books = await books_service.get_books()
    return mongo_to_vue_list(books, BookVue)


@router.get("/{book_id}", response_model=BookVue, description="Get a book")
async def get_book(book_id: PydanticObjectId) -> BookVue:
    book = await books_service.get_book(book_id)
    return mongo_to_vue(book.dict(), BookVue)


@router.get("/{book_id}/topics", description="Get all topics of a book",response_model=List[TopicVue])
async def get_book_topics(book_id: PydanticObjectId) -> List[TopicVue]:
    topics = await books_service.get_book_topics(book_id)
    return mongo_to_vue_list(topics, TopicVue)


@router.get("/{book_id}/topics/{topic_id}", description="Get a topic of a book")
async def get_book_topic(book_id: PydanticObjectId, topic_id: PydanticObjectId):
    return await books_service.get_book_topic(book_id, topic_id)


@router.get("/{book_id}/keywords", description="Get all keywords of a book",response_model=List[KeywordVue])
async def get_book_keywords(book_id: PydanticObjectId) -> List[KeywordVue]:
    keywords = await books_service.get_book_keywords(book_id)
    return mongo_to_vue_list(keywords, KeywordVue)


@router.get("/{book_id}/keywords/{keywordId}", description="Get a keyword of a book", response_model=KeywordVue)
async def get_book_keyword(book_id: PydanticObjectId, keyword_id: PydanticObjectId):
    keyword = await books_service.get_book_keyword(book_id, keyword_id)
    return mongo_to_vue(keyword.dict(), KeywordVue)


@router.get("/{book_id}/orders", response_model=List[OrderVue], description="Get all orders of a book")
async def get_book_orders(book_id : PydanticObjectId) -> List[OrderVue]:
    orders = await books_service.get_book_orders(book_id)
    return mongo_to_vue_list(orders, OrderVue)


@router.get("/{book_id}/orders/{orderId}", description="Get an order of a book", response_model=OrderVue)
async def get_book_order(book_id : PydanticObjectId, order_id : PydanticObjectId):
    order = await books_service.get_book_order(book_id, order_id)
    return mongo_to_vue(order.dict(), OrderVue)
