from beanie import PydanticObjectId
from fastapi import APIRouter

from services import customers_service

router = APIRouter(
    prefix="/auth",
    tags=["customers"],
)


@router.put("/role")
async def modify_role(token: str, role: str):
    return customers_service.modify_role(token, role)

