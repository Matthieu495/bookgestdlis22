from typing import List

from beanie import PydanticObjectId
from fastapi import APIRouter

from adapters.mongo_vue_adapter import vue_to_mongo, mongo_to_vue
from models.books import Book, UpdateBook
from services import books_service
from vues.book import BookVue

router = APIRouter(
    prefix="/books",
    tags=["books"],
)


@router.post("/", response_model=BookVue, description="Create a book")
async def post_book(book: BookVue) -> BookVue:
    book_mongo = vue_to_mongo(book.dict(), Book)
    book_mongo = await books_service.create_book(book_mongo)
    return mongo_to_vue(book_mongo.dict(), BookVue)


@router.patch("/{book_id}", response_model=BookVue, description="Update a book")
async def patch_book(book_id: PydanticObjectId, book_update: UpdateBook) -> BookVue:
    book = await books_service.update_book(book_id, book_update)
    return mongo_to_vue(book.dict(), BookVue)


@router.delete("/{book_id}", description="Delete a book")
async def delete_book(book_id: PydanticObjectId):
    await books_service.delete_book(book_id)


@router.delete("/{book_id}/topics/{topic_id}", description="Delete a topic of a book")
async def delete_book_topic(book_id: PydanticObjectId, topic_id: PydanticObjectId):
    await books_service.remove_book_topic(book_id, topic_id)


@router.delete("/{book_id}/keywords/{keyword_id}", description="Delete a keyword of a book")
async def delete_book_keyword(book_id: PydanticObjectId, keyword_id: PydanticObjectId):
    await books_service.remove_book_keyword(book_id, keyword_id)

@router.post("/{book_id}/topics/{topic_id}", description="Add a topic to a book")
async def add_book_topic(book_id: PydanticObjectId, topic_id: PydanticObjectId):
    await books_service.add_book_topic(book_id, topic_id)


@router.post("/{book_id}/keywords/{keyword_id}", description="Add a keyword to a book")
async def add_book_keyword(book_id: PydanticObjectId, keyword_id: PydanticObjectId):
    await books_service.add_book_keyword(book_id, keyword_id)