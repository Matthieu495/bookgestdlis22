from fastapi import APIRouter

from adapters.mongo_vue_adapter import mongo_to_vue
from models.keywords import Keyword
from services import keywords_service
from vues.keyword import KeywordVue

router = APIRouter(
    prefix="/keywords",
    tags=["keywords"],
)


@router.post("/")
async def post_keyword(keyword:KeywordVue):
    return keywords_service.create_keyword(mongo_to_vue(keyword.dict(),Keyword))
