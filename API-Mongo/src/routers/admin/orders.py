from fastapi import APIRouter, Depends

from adapters.mongo_vue_adapter import  mongo_to_vue_list
from internal.auth import JWTBearer
from services import orders_service
from vues.order import OrderVue

router = APIRouter(
    prefix="/orders",
    tags=["orders"],
)

@router.get("/", response_model=list[OrderVue], description="Get all orders")
async def get_orders() -> list[OrderVue]:
    orders = await orders_service.get_orders()
    return mongo_to_vue_list(orders, OrderVue)

