from typing import Optional

from beanie import PydanticObjectId
from pydantic import BaseModel


class BookVue(BaseModel):
    id: Optional[PydanticObjectId]
    title: str
    author: str
    resume: Optional[str]
    price: float
    image: Optional[str]
    stock: int
