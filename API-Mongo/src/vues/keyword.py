from beanie import PydanticObjectId
from pydantic import BaseModel


class KeywordVue(BaseModel):
    id: PydanticObjectId
    label: str