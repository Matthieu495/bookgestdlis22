from beanie import PydanticObjectId

from models.keywords import Keyword
from models.books import Book, UpdateBook
from fastapi import HTTPException

from models.orders import Order
from models.topics import Topic


async def create_book(book: Book) -> Book:
    return await book.create()


async def get_books() -> list[Book]:
    return await Book.find_all().to_list()


async def get_book(id: PydanticObjectId) -> Book:
    return await Book.get(id)


async def get_book_keywords(id: PydanticObjectId) -> list[Keyword]:
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    if not book.keywords:
        return []
    return book.keywords;


async def get_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId) -> Keyword:
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword not in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword not found")
    return keyword


async def get_book_topics(id: PydanticObjectId) -> list[Topic]:
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    if not book.topics:
        return []
    return book.topics;


async def update_book(id: PydanticObjectId, book_update: UpdateBook):
    book_update = {k: v for k, v in book_update.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in book_update.items()
    }}
    book = await Book.get(id)
    if not book:
        raise HTTPException(status_code=404, detail="Book not found")
    await book.update(update_query)
    return book


async def delete_book(id: PydanticObjectId):
    book = await Book.get(id)
    await book.delete()


async def get_book_orders(id: PydanticObjectId) -> list[Order]:
    return await Order.find_all(Order.book == id).to_list()


async def get_book_order(id: PydanticObjectId, order_id: PydanticObjectId) -> Order:
    return await Order.find_one(Order.book == id, Order.id == order_id)



async def remove_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId):
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword not in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword not found")
    await book.update({"$pull": {"keywords": keyword}})

async def add_book_keyword(id: PydanticObjectId, keyword_id: PydanticObjectId):
    book = await Book.get(id)
    keyword = await Keyword.get(keyword_id)
    if keyword in book.keywords:
        raise HTTPException(status_code=404, detail="Keyword already exists")
    await book.update({"$push": {"keywords": keyword}})

async def remove_book_topic(id: PydanticObjectId, topic_id: PydanticObjectId):
    book = await Book.get(id)
    topic = await Topic.get(topic_id)
    if topic not in book.topics:
        raise HTTPException(status_code=404, detail="Topic not found")
    await book.update({"$pull": {"topics": topic}})

async def add_book_topic(id: PydanticObjectId, topic_id: PydanticObjectId):
    book = await Book.get(id)
    topic = await Topic.get(topic_id)
    if topic in book.topics:
        raise HTTPException(status_code=404, detail="Topic already exists")
    await book.update({"$push": {"topics": topic}})

