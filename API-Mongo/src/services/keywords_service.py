from beanie import PydanticObjectId
from fastapi import HTTPException

from models.books import Book
from models.keywords import Keyword


async def get_keywords() -> list[Keyword]:
    return await Keyword.find_all().to_list()


async def get_keyword(id: PydanticObjectId) -> Keyword:
    return await Keyword.get(id)


async def get_keyword_books(id: PydanticObjectId) -> list[Book]:
    return await Book.find({'keywords._id': id}).to_list()
async def create_keyword(keyword: Keyword) -> Keyword:
    await keyword.create()
    return keyword