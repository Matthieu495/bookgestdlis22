from typing import List

from beanie import PydanticObjectId
from fastapi import HTTPException

from models.books import Book
from models.topics import Topic


async def create_topic(topic: Topic) -> Topic:
    return await topic.create()


async def get_topics() -> List[Topic]:
    return await Topic.find_all().to_list()


async def get_topic(id: PydanticObjectId) -> Topic:
    return await Topic.get(id)


async def get_topic_books(id: PydanticObjectId) -> List[Book]:
    topic = await Topic.get(id)
    if not topic:
        raise HTTPException(status_code=404, detail="Topic not found")
    if not topic.books:
        return []
    return topic.books
