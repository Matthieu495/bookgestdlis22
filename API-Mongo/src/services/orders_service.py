from beanie import PydanticObjectId

from models.orders import Order


async def get_orders() -> list[Order]:
    return await Order.find_all().to_list()

async def get_order(id: PydanticObjectId) -> Order:
    return await Order.get(id)

async def create_order(order: Order) -> Order:
    await order.create()
    return order

async def update_order(id: PydanticObjectId, order: Order):
    order_update = {k: v for k, v in order.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in order_update.items()
    }}
    order = await Order.get(id)
    await order.update(update_query)
    return order

async def delete_order(id: PydanticObjectId):
    order = await Order.get(id)
    await order.delete()





