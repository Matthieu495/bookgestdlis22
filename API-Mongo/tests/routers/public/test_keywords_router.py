from fastapi.testclient import TestClient

from main import app


# Créer un keyword et un book associé pour les tests suivants

def test_get_keywords():
    with TestClient(app) as client:
        response = client.get("keywords")
        assert response.status_code == 200


def test_get_keyword() :
    with TestClient(app) as client:
        response = client.get("keywords/6491680ddc3a98a4622dd2c5")
        assert response.status_code == 200


def test_get_keyword_books():
    assert True
    # with TestClient(app) as client:
    #     response = client.get("keywords/6491680ddc3a98a4622dd2c5/books")
    #     assert response.status_code == 200
