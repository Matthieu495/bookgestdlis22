from typing import Annotated

from fastapi import Depends, status
from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from ..config import get_db
from ..internal.auth_request import call_auth
from ..models import Customer as CustomerModel
from ..schemas import Customer as CustomerSchema

""" 
    This file contains the authentication logic for the API.
"""

# password hashing
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class JWTBearer(HTTPBearer):
    """ This class is used to verify the JWT token sent by the user. """

    def __init__(self, auto_error: bool = True):
        """
            This method is used to initialize the JWTBearer class.

            Parameters:
                auto_error (bool): If True, the class will raise an HTTPException if the token is invalid.
        """
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        """
            This method is used to verify the JWT token sent by the user.

            Parameters:
                request (Request): The request sent by the user.
        """
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if credentials.scheme != "Bearer":
                raise HTTPException(status_code=403, detail="Invalid authentication scheme.")
            current_user = self.verify_jwt(credentials.credentials)
            if not current_user:
                raise HTTPException(status_code=403, detail="Invalid token or expired token.")
            current_user['token'] = credentials.credentials
            return current_user
        else:
            raise HTTPException(status_code=403, detail="Invalid authorization code.")

    def verify_jwt(self, jwtoken: str):
        """
            This method is used to verify the JWT token sent by the user.

            Parameters:
                jwtoken (str): The JWT token sent by the user.
        """
        try:
            [response, response_content] = call_auth(method='get', path='user', token_user=jwtoken)
            if response.status_code != 200:
                raise HTTPException(status_code=response.status_code, detail=response_content)
        except HTTPException as e:
            response_content = None
            raise e
        return response_content


def get_current_customer(current_user: Annotated[CustomerSchema, Depends(JWTBearer())], db: Session = Depends(get_db)):
    """
        This method is used to get the current customer.

        Parameters:
            current_user (CustomerSchema): The current user.
            db (Session): The database session.
    """
    customer = db.query(CustomerModel).filter(CustomerModel.id_auth == current_user['id']).first()
    if customer is not None:
        current_user['cumulative_sales'] = customer.cumulative_sales
        return current_user
    else:
        raise HTTPException(status_code=404, detail="Customer not found")


def is_admin(current_user: Annotated[CustomerSchema, Depends(JWTBearer())]):
    """
        This method is used to check if the current user is an admin.

        Parameters:
            current_user (CustomerSchema): The current user.
    """
    if current_user["role"] == 'user':
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="You are not an admin"

        )
    return True
