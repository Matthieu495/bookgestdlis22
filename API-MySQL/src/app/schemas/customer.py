from typing import Optional, Union

from pydantic import BaseModel


class CustomerBase(BaseModel):
    email: str
    firstname: str
    lastname: str


class CustomerLogin(BaseModel):
    email: str
    password: str


class CustomerCreate(CustomerBase):
    password: str


class CustomerConfirmation(CustomerCreate):
    password_confirmation: str


class CustomerAuth(BaseModel):
    id_auth: str


class Customer(CustomerBase):
    id: int
    cumulative_sales: int

    class Config:
        orm_mode = True


class CustomerResponse(Customer):
    token: str


class CustomerPatch(BaseModel):
    id: str
    email: Union[str, None] = None
    password: Union[str, None] = None
    firstname: Union[str, None] = None
    lastname: Union[str, None] = None
    role: Union[str, None] = None


