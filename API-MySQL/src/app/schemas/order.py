from datetime import datetime
from pydantic import BaseModel, validator


class OrderBase(BaseModel):
    id_book: int
    id_customer: str
    quantity: int
    date: datetime = datetime.now()

    @validator('quantity')
    def validate_quantity(cls, quantity):
        if quantity <= 0:
            raise ValueError("Quantity must be greater than zero.")
        return quantity


class Order(OrderBase):
    id: int

    class Config:
        orm_mode = True
