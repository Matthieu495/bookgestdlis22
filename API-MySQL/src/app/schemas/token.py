from pydantic import BaseModel, validator


class Token(BaseModel):
    message: str
    token: str


class TokenData(BaseModel):
    id: int
