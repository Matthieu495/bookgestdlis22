from pydantic import BaseModel


class TopicBase(BaseModel):
    label: str
    image: str = ''


class TopicCreate(TopicBase):
    pass


class TopicUpdate(TopicBase):
    pass


class Topic(TopicBase):
    id: int

    class Config:
        orm_mode = True
