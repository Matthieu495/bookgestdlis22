import unittest
from datetime import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..models import Order

class OrderTestCase(unittest.TestCase):
    def setUp(self):
        # Configurer la connexion à la base de données de test
        engine = create_engine('sqlite:///test.db')
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Créer la table "orders" dans la base de données de test
        Order.metadata.create_all(engine)

    def tearDown(self):
        # Supprimer la table "orders" de la base de données de test
        Order.metadata.drop_all(self.session.bind)

    def test_order_representation(self):
        # Créer une commande de test
        order_date=datetime.now()
        order_date_str = str(order_date)
        order = Order(
            quantity=2,
            order_date=order_date
        )
        self.session.add(order)
        self.session.commit()

        # Vérifier que la représentation en chaîne de caractères est correcte
        expected_string = "Order : 1 - 2 - " + order_date_str
        self.assertEqual(repr(order), expected_string)

if __name__ == '__main__':
    unittest.main()