import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from ..models import Keyword

class KeywordTestCase(unittest.TestCase):
    def setUp(self):
        # Configurer la connexion à la base de données de test
        engine = create_engine('sqlite:///test.db')
        Session = sessionmaker(bind=engine)
        self.session = Session()

        # Créer la table "keywords" dans la base de données de test
        Keyword.metadata.create_all(engine)

    def tearDown(self):
        # Supprimer la table "keywords" de la base de données de test
        Keyword.metadata.drop_all(self.session.bind)

    def test_keyword_representation(self):
        # Créer un mot-clé de test
        keyword = Keyword(label="Science Fiction")
        self.session.add(keyword)
        self.session.commit()

        # Vérifier que la représentation en chaîne de caractères est correcte
        expected_string = "Keyword : 1 - Science Fiction"
        self.assertEqual(repr(keyword), expected_string)

if __name__ == '__main__':
    unittest.main()