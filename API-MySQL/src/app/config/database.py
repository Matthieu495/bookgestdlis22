from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os

""" This file is used to connect to the database and create a session. """

load_dotenv()
# Connect to database
engine = create_engine(os.environ['DATABASE_URL'])
# Create a session
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
# Create a base class
Base = declarative_base()


# Singleton pattern for database
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
