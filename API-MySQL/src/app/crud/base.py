from typing import Any

from fastapi import HTTPException
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from ..config import Base

""" This file contains the CRUD operations for the database. """


def delete_one_by_id(id: int, model: Base, db: Session):
    """
        Deletes an entity based on its ID.

        :param id: The ID of the entity to be deleted.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :return: The deleted entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    entity = db.query(model).filter(model.id == id).first()
    if entity:
        db.delete(entity)
        db.commit()
        return entity
    else:
        db.rollback()
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def get_all(model: Base, db, offset: int = 0, limit: int = 100):
    """
        Gets all entities of a model.

        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param offset: The offset of the query.
        :type offset: int
        :param limit: The limit of the query.
        :type limit: int
        :return: The entities.
        :rtype: list
    """
    return db.query(model).offset(offset).limit(limit).all()


def get_one_by_id(id: int, model: Base, db):
    """
        Gets an entity based on its ID.

        :param id: The ID of the entity to be retrieved.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :return: The entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    entity = db.query(model).filter(model.id == id).first()
    if entity:
        return entity
    else:
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def get_one_by_fields(db: Session, model: Base, **kwargs):
    """
        Gets an entity based on its fields.

        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param kwargs: The fields of the entity to be retrieved.
        :type kwargs: dict
        :return: The entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    try:
        return db.query(model).filter_by(**kwargs).first()
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


def patch_one_by_id(id: int, model: Base, db: Session, **kwargs):
    """
        Updates an entity based on its ID.

        :param id: The ID of the entity to be updated.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param kwargs: The fields of the entity to be updated.
        :type kwargs: dict
        :return: The updated entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    db.query(model).filter(model.id == id).update(kwargs)
    db.commit()
    entity = db.query(model).filter(model.id == id).first()
    if entity:
        return entity
    else:
        db.rollback()
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def post_one(model: Base, db: Session, **kwargs):
    """
        Creates an entity.

        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param kwargs: The fields of the entity to be created.
        :type kwargs: dict
        :return: The created entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    db_object = model(**kwargs)
    db.add(db_object)
    try:
        db.commit()
        db.refresh(db_object)
    except IntegrityError:
        db.rollback()
        raise HTTPException(status_code=409, detail='Duplicate key error occurred.')
    return db_object


def get_all_relationships_by_id(id: int, db: Session, model: Base, relationship_field):
    """
        Gets all relationships of an entity based on its ID.

        :param id: The ID of the entity to be retrieved.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param relationship_field: The relationship field of the entity to be retrieved.
        :type relationship_field: str
        :return: The relationships.
        :rtype: list
        :raises HTTPException: If the entity is not found.
    """
    entity = get_one_by_id(db=db, id=id, model=model)
    if entity:
        return getattr(entity, relationship_field)
    else:
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def get_one_relationship_by_id(id: int, external_id: int, db: Session, model: Base, relationship_field):
    """
        Gets a relationship of an entity based on its ID.

        :param id: The ID of the entity to be retrieved.
        :type id: int
        :param external_id: The ID of the relationship to be retrieved.
        :type external_id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param relationship_field: The relationship field of the entity to be retrieved.
        :type relationship_field: str
        :return: The relationship.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    entity = get_one_by_id(db=db, id=id, model=model)
    if entity:
        relationships = getattr(entity, relationship_field)
        for relationship in relationships:
            if relationship.id == external_id:
                return relationship
    else:
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")

    ############ A REFACTORISER ################


def get_one_by_id_auth(id: int, model: Base, db):
    """
        Gets an entity based on its ID_AUTH.

        :param id: The ID_AUTH of the entity to be retrieved.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :return: The entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    entity = db.query(model).filter(model.id_auth == id).first()
    if entity:
        return entity
    else:
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def get_all_relationships_by_id_auth(id: str, db: Session, model: Base, relationship_field):
    """
        Gets all relationships of an entity based on its ID_AUTH.

        :param id: The ID_AUTH of the entity to be retrieved.
        :type id: str
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param relationship_field: The relationship field of the entity to be retrieved.
        :type relationship_field: str
        :return: The relationships.
        :rtype: list
        :raises HTTPException: If the entity is not found.
    """
    entity = get_one_by_id_auth(db=db, id=id, model=model)
    if entity:
        return getattr(entity, relationship_field)
    else:
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")


def patch_one_by_id_auth(id: int, model: Base, db: Session, **kwargs):
    """
        Updates an entity based on its ID_AUTH.

        :param id: The ID_AUTH of the entity to be updated.
        :type id: int
        :param model: entity model.
        :type model: sqlalchemy.ext.declarative.api.Base
        :param db: The database session.
        :type db: sqlalchemy.orm.session.Session
        :param kwargs: The fields of the entity to be updated.
        :type kwargs: dict
        :return: The updated entity.
        :rtype: sqlalchemy.ext.declarative.api.Base
        :raises HTTPException: If the entity is not found.
    """
    db.query(model).filter(model.id_auth == id).update(kwargs)
    db.commit()
    entity = db.query(model).filter(model.id_auth == id).first()
    if entity:
        return entity
    else:
        db.rollback()
        model_name = model.__name__
        raise HTTPException(status_code=404, detail=f"{model_name} not found")
