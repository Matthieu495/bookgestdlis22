from typing import Annotated

from fastapi import APIRouter, Depends
from fastapi.params import Path
from sqlalchemy.orm import Session

from ..config import get_db
from ..crud import get_one_by_id, get_all, patch_one_by_id, post_one, delete_one_by_id
from ..internal import JWTBearer
from ..internal import is_admin
from ..models import Keyword, Customer
from ..schemas import Keyword as KeywordModel, KeywordBase

router = APIRouter()


@router.post('/', response_model=KeywordModel)
def create_keyword(keyword: KeywordBase,
                   current_user: Annotated[Customer, Depends(JWTBearer())],
                   db: Session = Depends(get_db)
                   ):
    """
        Create a new keyword

        - **keyword**: The keyword to create
        - **current_user**: The current user (must be admin)
        - **db**: The database session
    """
    if is_admin(current_user):
        return post_one(model=Keyword, db=db, **keyword.dict())


@router.delete('/{keyword_id}', response_model=KeywordModel)
def delete_keyword(keyword_id: Annotated[int, Path(title="The ID of the keyword to delete")],
                   current_user: Annotated[Customer, Depends(JWTBearer())],
                   db: Session = Depends(get_db)
                   ):
    """
        Delete a keyword

        - **keyword_id**: The ID of the keyword to delete
        - **current_user**: The current user (must be admin)
        - **db**: The database session
    """
    if is_admin(current_user):
        return delete_one_by_id(id=keyword_id, model=Keyword, db=db)


@router.get('/{keyword_id}', response_model=KeywordModel)
def get_keyword(keyword_id: Annotated[int, Path(title="The ID of the keyword to get")], db: Session = Depends(get_db)):
    """
        Get a keyword by ID

        - **keyword_id**: The ID of the keyword to get
        - **db**: The database session
    """
    return get_one_by_id(id=keyword_id, model=Keyword, db=db)


@router.get('/', response_model=list[KeywordModel])
def get_all_keywords(limit: int = 100, offset: int = 0, db: Session = Depends(get_db)):
    """
        Get all keywords

        - **limit**: The maximum number of keywords to get
        - **offset**: The number of keywords to skip
        - **db**: The database session
    """
    return get_all(model=Keyword, db=db, limit=limit, offset=offset)


@router.patch('/{keyword_id}', response_model=KeywordModel)
def patch_keyword(keyword_id: Annotated[int, Path(title="The ID of the keyword to patch")], keyword: KeywordBase,
                  current_user: Annotated[Customer, Depends(JWTBearer())],
                  db: Session = Depends(get_db)
                  ):
    """
        Patch a keyword

        - **keyword_id**: The ID of the keyword to patch
        - **keyword**: The keyword to patch
        - **current_user**: The current user (must be admin)
        - **db**: The database session
    """
    if is_admin(current_user):
        return patch_one_by_id(id=keyword_id, model=Keyword, db=db, **keyword.dict())
