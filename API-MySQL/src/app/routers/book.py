from typing import Annotated, List, Union

from fastapi import APIRouter, Depends, BackgroundTasks, HTTPException, Query
from fastapi.params import Path
from sqlalchemy import insert, delete, select, func
from sqlalchemy.orm import Session
from starlette import status

from ..config import get_db
from ..crud import get_one_by_id, get_all, patch_one_by_id, post_one, delete_one_by_id, get_all_relationships_by_id, \
    get_one_by_fields

try:
    from ..elastic import es_update_book, es_delete_book, es_add_book
except ImportError:
    es_update_book = None
    es_delete_book = None
    es_add_book = None
from ..internal import is_admin, JWTBearer
from ..models import Book, Customer, book_topic, book_keyword, Keyword as KeywordModel
from ..schemas import Book as BookSchema, BookBase, Keyword, Topic, BookTopic, BookKeyword

router = APIRouter()


@router.post('/', response_model=BookSchema)
def create_book(book: BookBase,
                background_tasks: BackgroundTasks,
                current_user: Annotated[Customer, Depends(JWTBearer())],
                db: Session = Depends(get_db)
                ):
    """
        Create a new book

        :param book: BookBase
        :param background_tasks: BackgroundTasks
        :param current_user: Customer token
        :param db: Session
    """
    if is_admin(current_user):
        # Check if book already exists
        book_in_stock = get_one_by_fields(model=Book, db=db, author=book.author, title=book.title)
        if book_in_stock is not None:
            # Update book stock
            stock = book_in_stock.stock + book.stock
            updated_book = patch_one_by_id(id=book_in_stock.id, model=Book, db=db, stock=stock, price=book.price)
            if es_update_book is not None:
                background_tasks.add_task(es_update_book, book_in_stock.id, updated_book)
            return book_in_stock
        else:
            # Create a new book
            new_book = post_one(model=Book, db=db, **book.dict())
            if es_add_book is not None:
                background_tasks.add_task(es_add_book, new_book)
            return new_book


@router.post('/{book_id}/topics/{topic_id}', response_model=BookTopic)
def create_book_topic(book_id: int,
                      topic_id: int,
                      current_user: Annotated[Customer, Depends(JWTBearer())],
                      db: Session = Depends(get_db)
                      ):
    """
        Add a topic to a book

        :param book_id: int
        :param topic_id: int
        :param current_user: Customer token
        :param db: Session
    """
    book_topic_schema = BookTopic(id_book=book_id, id_topic=topic_id)
    if is_admin(current_user):
        # Check if book or topic exists
        if book_topic_schema.id_book == 0 or book_topic_schema.id_topic == 0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="ID of book or topic is invalid")
        db.execute(insert(book_keyword), book_topic_schema.dict())
        db.commit()
        return book_topic_schema


@router.post('/{book_id}/keywords/{keyword_id}', response_model=BookKeyword)
def create_book_keyword(book_id: int,
                        keyword_id: int,
                        current_user: Annotated[Customer, Depends(JWTBearer())],
                        db: Session = Depends(get_db)
                        ):
    """
        Add a keyword to a book

        :param book_id: int
        :param keyword_id: int
        :param current_user: Customer token
        :param db: Session
    """
    book_keyboard_schema = BookKeyword(id_book=book_id, id_keyword=keyword_id)
    if is_admin(current_user):
        if book_keyboard_schema.id_book == 0 or book_keyboard_schema.id_keyword == 0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="ID of book or keyword is invalid")
        db.execute(insert(book_keyword), book_keyboard_schema.dict())
        db.commit()
        return book_keyboard_schema


@router.delete('/{book_id}', response_model=BookSchema)
def delete_book(book_id: Annotated[int, Path(title="The ID of the book to delete")],
                background_tasks: BackgroundTasks,
                current_user: Annotated[Customer, Depends(JWTBearer())],
                db: Session = Depends(get_db)
                ):
    """
        Delete a book

        :param book_id: int
        :param background_tasks: BackgroundTasks
        :param current_user: Customer token
        :param db: Session
    """
    if is_admin(current_user):
        if es_delete_book is not None:
            background_tasks.add_task(es_delete_book, book_id)
        return delete_one_by_id(id=book_id, model=Book, db=db)


@router.delete('/{book_id}/topics/{topic_id}', response_model=BookTopic)
def delete_book_topic(book_topic_schema: BookTopic,
                      current_user: Annotated[Customer, Depends(JWTBearer())],
                      db: Session = Depends(get_db)
                      ):
    """
        Delete a topic from a book

        :param book_topic_schema: BookTopic
        :param current_user: Customer token
        :param db: Session
    """
    if is_admin(current_user):
        if book_topic_schema.id_book == 0 or book_topic_schema.id_topic == 0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="ID of book or topic is invalid")
        stmt = delete(book_topic).where(
            book_topic.c.id_book == book_topic_schema.id_book,
            book_topic.c.id_topic == book_topic_schema.id_topic
        )
        db.execute(stmt)
        db.commit()
        return book_topic_schema


@router.delete('/{book_id}/keywords/{keyword_id}', response_model=BookKeyword)
def delete_book_keyword(book_keyword_schema: BookKeyword,
                        current_user: Annotated[Customer, Depends(JWTBearer())],
                        db: Session = Depends(get_db)
                        ):
    """
        Delete a keyword from a book

        :param book_keyword_schema: BookKeyword
        :param current_user: Customer token
        :param db: Session
    """
    if is_admin(current_user):
        if book_keyword_schema.id_book == 0 or book_keyword_schema.id_keyword == 0:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail="ID of book or keyword is invalid")
        stmt = delete(book_keyword).where(
            book_keyword.c.id_book == book_keyword_schema.id_book,
            book_keyword.c.id_keyword == book_keyword_schema.id_keyword
        )
        db.execute(stmt)
        db.commit()
        return book_keyword_schema


@router.get('/')
def get_all_books(operator: Union[str, None] = None, keyword: Annotated[Union[List[int], None], Query()] = None,
                  limit: int = 100, offset: int = 0, db: Session = Depends(get_db)):
    """
        Get all books

        :param operator: str
        :param keyword: list of int
        :param limit: int
        :param offset: int
        :param db: Session
    """
    if keyword is None or operator is None:
        books = get_all(model=Book, db=db, limit=limit, offset=offset)
    elif keyword is not None and operator is not None:
        operator = operator.lower()
        if operator == "or":
            stmt = select(Book).distinct(Book.id).join(Book.keywords).where(KeywordModel.id.in_(keyword))
        elif operator == "and":
            stmt = select(Book).distinct(Book.id).join(Book.keywords).where(KeywordModel.id.in_(keyword)).group_by(
                Book.id).having(func.count() == len(keyword))
        elif operator == "not":
            stmt = select(Book).distinct(Book.id).join(Book.keywords).where(
                ~Book.keywords.any(KeywordModel.id.in_(keyword)))

        results = db.execute(stmt.limit(limit).offset(offset)).all()
        books = [row[0] for row in results]
    else:
        books = []

    return books


@router.get('/{book_id}', response_model=BookSchema)
def get_book(book_id: Annotated[int, Path(title="The ID of the book to get")], db: Session = Depends(get_db)):
    """
        Get a book by ID

        :param book_id: int
        :param db: Session
    """
    return get_one_by_id(id=book_id, model=Book, db=db)


@router.get("/{book_id}/keywords", response_model=list[Keyword])
def get_book_keywords(book_id: int, db: Session = Depends(get_db)):
    """
        Get all keywords of a book

        :param book_id: int
        :param db: Session
    """
    return get_all_relationships_by_id(db=db, id=book_id, model=Book, relationship_field='keywords')


@router.get("/{book_id}/topics", response_model=list[Topic])
def get_book_topics(book_id: int, db: Session = Depends(get_db)):
    """
        Get all topics of a book

        :param book_id: int
        :param db: Session
    """
    return get_all_relationships_by_id(db=db, id=book_id, model=Book, relationship_field='topics')


@router.patch('/{book_id}', response_model=BookSchema)
def patch_book(book_id: Annotated[int, Path(title="The ID of the book to patch")],
               background_tasks: BackgroundTasks,
               book: BookBase,
               current_user: Annotated[Customer, Depends(JWTBearer())],
               db: Session = Depends(get_db)
               ):
    """
        Patch a book

        :param book_id: int
        :param background_tasks: BackgroundTasks
        :param book: BookBase
        :param current_user: Customer token
        :param db: Session
    """
    if is_admin(current_user):
        updated_book = patch_one_by_id(id=book_id, model=Book, db=db, **book.dict())
        if es_update_book is not None:
            background_tasks.add_task(es_update_book, book_id, updated_book)
        return updated_book
