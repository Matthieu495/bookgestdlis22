from elasticsearch import Elasticsearch
from fastapi import APIRouter, Depends, Query

from ..elastic import es_auth, CUSTOMER_INDEX_NAME, BOOK_INDEX_NAME

router = APIRouter()


def get_es_client():
    """ Get an Elasticsearch client instance. """
    es_client = Elasticsearch(
        es_auth.host,
        basic_auth=(es_auth.user, es_auth.password.get_secret_value()),
    )

    try:
        yield es_client
    finally:
        es_client.close()


def search_documents(query: str,
                     index_name: str,
                     fields: list[str],
                     es_client: Elasticsearch = Depends(get_es_client),
                     ):
    """
        Search documents in the Elasticsearch index.

        :param query: The search query.
        :param index_name: The name of the Elasticsearch index.
        :param fields: The fields to search in.
        :param es_client: The Elasticsearch client instance.
    """
    if len(query.strip()) == 0:
        search_query = {
            "match_all": {}
        }
    else:
        search_query = {
            "multi_match": {
                "query": query,
                "type": "most_fields",
                "operator": "or",
                "fields": fields,
                "fuzziness": "AUTO"
            }
        }

    results = es_client.search(
        index=index_name,
        query=search_query,
        size=1000
    )

    documents_found = [
        hit["_source"] for hit in results["hits"]["hits"]
    ]

    return documents_found


@router.get("/customer")
async def search_customer_documents(query: str = Query(alias="q"),
                                    es_client: Elasticsearch = Depends(get_es_client),
                                    ):
    """
        Search customer documents in the Elasticsearch index.

        :param query: The search query.
        :param es_client: The Elasticsearch client instance.
    """
    fields = [
        "lastname^3",
        "lastname.ngrams",
        "firstname^2",
        "firstname.ngrams",
    ]

    documents_found = search_documents(
        query=query,
        index_name=CUSTOMER_INDEX_NAME,
        fields=fields,
        es_client=es_client,
    )

    return documents_found


@router.get("/book")
async def search_book_documents(query: str = Query(alias="q"),
                                es_client: Elasticsearch = Depends(get_es_client),
                                ):
    """
        Search book documents in the Elasticsearch index.

        :param query: The search query.
        :param es_client: The Elasticsearch client instance.
    """
    fields = [
        "title^3",
        "title.ngrams",
        "author^3",
        "author.ngrams",
        "topic_labels^2",
        "keyword_labels^2",
    ]

    documents_found = search_documents(
        query=query,
        index_name=BOOK_INDEX_NAME,
        fields=fields,
        es_client=es_client,
    )
    return documents_found
