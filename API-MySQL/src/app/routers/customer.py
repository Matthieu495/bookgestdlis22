from typing import Annotated
import os
import requests

from dotenv import load_dotenv
from fastapi import APIRouter, Depends, BackgroundTasks
from fastapi.params import Path
from sqlalchemy.orm import Session
from starlette.responses import JSONResponse

from ..internal import call_auth, get_current_customer
from ..config import get_db
from ..crud import get_all_relationships_by_id_auth

try:
    from ..elastic import es_update_customer, es_delete_customer
except ImportError:
    es_update_customer = None
    es_delete_customer = None
from ..internal import JWTBearer
from ..internal import is_admin
from ..models import Customer
from ..schemas import CustomerPatch, Customer as CustomerSchema, Order

router = APIRouter()
load_dotenv()


@router.delete('/{id_customer}', response_class=JSONResponse)
def delete_customer(id_customer: Annotated[str, Path(title="The ID of the customer to delete")],
                    background_tasks: BackgroundTasks,
                    current_user: Annotated[Customer, Depends(JWTBearer())]
                    ):
    """
        Delete a customer and all his orders.

        Args:
            id_customer (str): The ID of the customer to delete.
            background_tasks (BackgroundTasks): BackgroundTasks object.
            current_user (Customer): The current user token.
    """
    if is_admin(current_user):
        [response, response_content] = call_auth(method='delete', path='user', json={'id': id_customer},
                                                 token_user=current_user['token'])
        return JSONResponse(status_code=response.status_code, content=response_content)


@router.get('/current')
def get_current_customer(current_user: Annotated[Customer, Depends(get_current_customer)]):
    """ Get the current customer. """
    return current_user


@router.get('/{id_customer}', response_class=JSONResponse)
def get_customer(id_customer: Annotated[str, Path(title="The ID of the customer to get")],
                 current_user: Annotated[Customer, Depends(JWTBearer())]
                 ):
    """
        Get a customer by ID.

        Args:
            id_customer (str): The ID of the customer to get.
            current_user (Customer): The current user token.
    """
    if is_admin(current_user):
        [response, response_content] = call_auth(method='get', path='user/' + id_customer,
                                                 token_user=current_user['token'])
        return JSONResponse(status_code=response.status_code, content=response_content)


@router.get('/', response_class=JSONResponse)
def get_all_customers(current_user: Annotated[Customer, Depends(JWTBearer())]):
    """
        Get all customers.

        Args:
            current_user (Customer): The current user token.
    """
    if is_admin(current_user):
        [response, response_content] = call_auth(method='get', path='users', token_user=current_user['token'])
        return JSONResponse(status_code=response.status_code, content=response_content)


@router.get("/{id_customer}/orders", response_model=list[Order])
def get_customer_orders(id_customer: str,
                        current_user: Annotated[Customer, Depends(JWTBearer())],
                        db: Session = Depends(get_db)
                        ):
    """
        Get all orders of a customer.

        Args:
            id_customer (str): The ID of the customer to get.
            current_user (Customer): The current user token.
            db (Session, optional): The database session. Defaults to Depends(get_db).
    """
    return get_all_relationships_by_id_auth(db=db, id=id_customer, model=Customer, relationship_field='orders')


@router.patch('/', response_model=CustomerSchema)
def patch_customer(customer: CustomerPatch,
                   current_user: Annotated[Customer, Depends(JWTBearer())],
                   ):
    """
        Update a customer.

        Args:
            customer (CustomerPatch): The customer to update.
            current_user (Customer): The current user token.
    """
    [response, response_content] = call_auth(method='patch', path='user', token_user=current_user['token'],
                                             json=customer.dict(exclude_none=True))

    return JSONResponse(status_code=response.status_code, content=response_content)
