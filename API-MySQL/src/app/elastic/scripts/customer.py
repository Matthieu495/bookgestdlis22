""" Customer index settings and mappings """
CUSTOMER_INDEX_NAME = "customer"
CUSTOMER_INDEX_SETTINGS = {
    "analysis": {
        "analyzer": {
            "post_index_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "lowercase",
                    "autocomplete_filter",
                ],
            },
            "post_search_analyzer": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "lowercase",
                ],
            },
        },
        "filter": {
            "autocomplete_filter": {
                "type": "edge_ngram",
                "min_gram": 1,
                "max_gram": 20,
            },
        },
    },
}

CUSTOMER_INDEX_MAPPINGS = {
    "properties": {
        "lastname": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
            "fields": {
                "ngrams": {
                    "type": "text",
                    "analyzer": "post_index_analyzer",
                    "search_analyzer": "post_search_analyzer",
                },
            },
        },
        "firstname": {
            "type": "text",
            "search_analyzer": "post_search_analyzer",
            "fields": {
                "ngrams": {
                    "type": "text",
                    "analyzer": "post_index_analyzer",
                    "search_analyzer": "post_search_analyzer",
                },
            },
        },
    }
}


