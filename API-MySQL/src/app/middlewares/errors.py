import logging
import os
""" Middleware to log errors in a file """
# Path to the log file
log_file_path = os.path.join(os.path.dirname(__file__), "..", "logs", "errors.log")
# Create the log file if it does not exist
logging.basicConfig(level=logging.ERROR, filename=log_file_path, filemode="a",
                    format="%(asctime)s - %(levelname)s - %(message)s")


async def error_log_middleware(request, call_next):
    """
        Middleware to log errors in a file

        :param request: request
        :param call_next: call_next
    """
    try:
        return await call_next(request)
    except Exception as e:
        logging.error(str(e))
        raise
