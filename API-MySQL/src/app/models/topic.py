from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from . import book_topic
from ..config import Base


class Topic(Base):
    __tablename__ = "topics"

    id = Column(Integer, primary_key=True, index=True)
    label = Column(String(255))
    image = Column(String(255))

    books = relationship("Book", secondary=book_topic, back_populates="topics")

    def __repr__(self):
        return f"Topic : {self.id} - {self.label} - {self.image}"
