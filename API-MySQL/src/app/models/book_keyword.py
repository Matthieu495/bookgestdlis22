from sqlalchemy import Column, Integer, ForeignKey, Table

from ..config import Base

book_keyword = Table(
    "book_keyword",
    Base.metadata,
    Column("id_book", Integer, ForeignKey("books.id")),
    Column("id_keyword", Integer, ForeignKey("keywords.id"))
)
