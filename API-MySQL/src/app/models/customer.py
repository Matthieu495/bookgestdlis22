from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.orm import relationship

from ..config import Base


class Customer(Base):
    __tablename__ = "customers"

    id = Column(Integer, primary_key=True, index=True)
    id_auth = Column(String(255), index=True)
    cumulative_sales = Column(Float, default=0)

    orders = relationship("Order", back_populates="customers")

    def __repr__(self):
        return f"Customer : {self.id} - {self.id_auth} - {self.cumulative_sales}"
