from sqlalchemy import Column, Integer, ForeignKey, Table

from ..config import Base

book_topic = Table(
    "book_topic",
    Base.metadata,
    Column("id_book", Integer, ForeignKey("books.id")),
    Column("id_topic", Integer, ForeignKey("topics.id"))
)
