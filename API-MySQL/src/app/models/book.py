from sqlalchemy import Column, Integer, String, Text, Float
from sqlalchemy.orm import relationship

from .book_topic import book_topic
from .book_keyword import book_keyword
from ..config import Base


class Book(Base):
    __tablename__ = "books"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(255))
    author = Column(String(255))
    resume = Column(String(255))
    image = Column(Text)
    price = Column(Float)
    stock = Column(Integer)

    orders = relationship("Order", back_populates="books")
    topics = relationship("Topic", secondary=book_topic, back_populates="books")
    keywords = relationship("Keyword", secondary=book_keyword, back_populates="books")

    def __str__(self):
        return f"Book : {self.id} - {self.title} - {self.author} - {self.price}"
