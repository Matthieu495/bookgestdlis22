#!/bin/bash

# Fonction pour démarrer les conteneurs
start_containers() {
  echo "Démarrage des conteneurs..."
  docker-compose up -d
}

# Fonction pour arrêter les conteneurs
stop_containers() {
  echo "Arrêt des conteneurs..."
  docker-compose down
}

# Vérifier le nombre d'arguments passés au script
if [[ $# -ne 1 ]]; then
  echo "Utilisation : ./run.sh [start|stop]"
  exit 1
fi

# Récupérer l'action à effectuer
action=$1

# Vérifier l'action spécifiée
if [[ $action == "start" ]]; then
  start_containers
elif [[ $action == "stop" ]]; then
  stop_containers
else
  echo "Action non valide. Utilisation : ./run.sh [start|stop]"
  exit 1
fi
