# BookGestdlis22

Le service d'authentification à pour objectif de gérer la partie authentification du projet FNUC.

## Author

VOILLET Hugo, LAIDIN Philippe, GABORIEAU Jonathan

## Getting started

- Importer le projet en faisant dans un terminal : git@gitlab.com:Matthieu495/bookgestdlis22.git

- Une fois le projet importer, il faut se rendre dans le dossier auth

- Ensuite, installer le container Docker en faisant : docker-compose up

## Documentations

Vous pourrez retrouvez des documentations du projet dans le dossier "documentation"

- Documentation de l'ancien projet : Rapport_Projet.pdf

- Recette de sprint semaine 1 : GrSAUTH_Recette_Sprint_1.pdf

- Recette de sprint semaine 2 : GrSAUTH_Recette_Sprint_2.pdf

- Rapport de test : GrSAUTH_Rapport_test.pdf

- Rapport daily meeting sprint 1 : GrSAUTH_Rapport_daily_meeting_sprint_1.pdf

- Rapport daily meeting sprint 2 : GrSAUTH_Rapport_daily_meeting_sprint_2.pdf

- Documentation/Spécification autres groupes : GrSAUTH*Documentation_Spécification_autres_groupe.pdf
  \_NB :*
  _- Le code est commenté_
  _- Des fichiers RestructuredText sont disponibles sous forme brute ou html_
