from flask import Flask,jsonify,request,render_template
from flask_cors import CORS, cross_origin
from waitress import serve
from utils.token import Token
from utils.user import User
from utils.password import Password
from pymongo.mongo_client import MongoClient
from bson.objectid import ObjectId
import json

app = Flask(__name__)

# URI de la base de données
uri = "mongodb+srv://admin:admin@fnucauth.x362oex.mongodb.net/?retryWrites=true&w=majority"

cors = CORS(app, resources={r"/*": {"origins": "*"}})

# Connexion à la base et récupération de la collection 'users'
client = MongoClient(uri)
db = client["AUTH_DB"]

users  = db["users"]

token_manager = Token()
password_manager = Password()

# Route simple permettant notamment de savoir si le serveur est bien lancé
@app.route("/")
def index():
    return "Bienvenue sur le serveur d'authentification !"

# Route affichant le swagger
@app.route("/docs")
def swagger():
    return render_template("swagger.html")

# Route d'inscription
@app.route("/register", methods=["POST"])
def register():
    import re

    data = request.get_json()
    
    # Vérification que les champs sont bien renseignés
    required_fields = ["lastname", "firstname", "email", "password","password_confirmation"]
    missing_fields = [field for field in required_fields if field not in data]
    if missing_fields:
        return jsonify({"message": f"Champs requis manquants : {', '.join(missing_fields)}"}), 400

    lastname = data["lastname"]
    firstname = data["firstname"]
    email = data["email"]
    password = data["password"]
    password_confirmation = data["password_confirmation"]
    # Vérifier que les mots de passe correspondent
    if password != password_confirmation:
        return jsonify({"message": "Les mots de passes sont différents"}), 400
    
    # Vérifier le format de l'email
    email_regex = (r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$")
    if not re.search(email_regex, email):
        return jsonify({"message": "Email invalide"}), 400

    # Vérifier le format du mot de passe : au moins une majuscule, un caractère spécial, et un chiffre
    password_regex = (r"^(?=.*[A-Z])(?=.*\d)(?=.*[@$!-_%*#?&])[A-Za-z\d@$!-_%*#?&]{8,}$")
    if not re.search(password_regex, password):
        return jsonify({"message": "Le mot de passe doit contenir au moins 8 caractères, dont au moins 1 chiffre, 1 caractère spécial, 1 lettre majuscule et minuscule"}), 400

    # Vérifier si l'utilisateur existe déjà dans la base de données
    if users.find_one({"email": email}):
        return jsonify({"message": "Un utilisateur avec cet email existe déjà"}), 409

    # Hacher le mot de passe
    hashed_password, salt = password_manager.hash_password(password)

    # Créer un nouvel objet User avec le mot de passe haché
    new_user = User(email=email, lastname=lastname, firstname=firstname, password=hashed_password, salt=salt, role='user', token=None)

    # Insérer l'utilisateur dans la base de données
    result = users.insert_one(new_user.__dict__)

    # Génération et ajout du token sur l'utilisateur en base
    user = users.find_one({"_id": ObjectId(result.inserted_id)})
    token = token_manager.generate_token(user)
    users.update_one({"_id": user["_id"]}, {"$set": {"token": token}})

    # Vérifier si l'insertion a réussi
    if result.inserted_id:
        return jsonify({"message": "Inscription réussie","token": token}), 201
    else:
        return jsonify({"message": "Échec de l'inscription"}), 500


# Route de connexion
@app.route("/login", methods=["POST"])
def login():
    data = request.get_json()
    email = data["email"]
    password = data["password"]

    user = users.find_one({"email": email})
    # Vérification que l'utilisateur existe
    if user:
        hashed_password = user.get("password", "")
        salt = user.get("salt", "")

        # Vérifier le mot de passe
        if password_manager.verify_password(password, hashed_password, salt):
            token = token_manager.generate_token(user)
            users.update_one({"_id": user["_id"]}, {"$set": {"token": token}})
            return jsonify({"message": "Connexion réussie", "token": token}), 200
        else:
            return jsonify({"message": "Échec de la connexion : Mot de passe incorrect"}), 401

    return jsonify({"message": "Échec de la connexion : Utilisateur introuvable"}), 401


# Route de deconnexion
@app.route("/logout", methods=["POST"])
def logout():
    
    token = request.data.decode("utf-8")
    payload,status = token_manager.get_payload(token)
    if payload and status == 'valid':
        user_id,status = token_manager.get_user_id_from_token(token)
        users.update_one({"_id": user_id}, {"$set": {"token": None}})
        return jsonify({"message": "Déconnexion réussie"}), 200
    elif status == 'expired':
        return jsonify({"message": "Le token a expiré"}), 401
    else:
        return jsonify({"message": "Le token est invalide"}), 401

# Route pour récupérer les infos d'un utilisateur en lui donnant un id dans l'url
@app.route("/user/<id>", methods=["GET"])
def get_one_user(id):    
    user = users.find_one({"_id": ObjectId(id)})
    if user:
        return jsonify({
            "id": str(user["_id"]),
            "email": user["email"],
            "lastname": user["lastname"],
            "firstname": user["firstname"],
            "role": user["role"]
        })
    else:
        return jsonify({"error": "Utilisateur introuvable"}), 404

# Route pour récupérer les infos d'un'utilisateur
@app.route("/user", methods=["GET"])
def get_user():
    # Récupération du token
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return jsonify({"error": "Token non fourni"}), 403

    token_parts = auth_header.split(" ")
    if len(token_parts) != 2 or token_parts[0].lower() != "bearer":
        return jsonify({"error": "Schéma d'authentification non supporté"}), 401

    token = token_parts[1]
    
    # Récupération de l'id
    user_id,status = token_manager.get_user_id_from_token(token)
    if user_id:
        # Récupération des informations de l'utilisateur
        user = users.find_one({"_id": user_id})

        if user:
            if user["token"] is  None:
                return jsonify({"error": "Utilisateur déconnecté"}), 404
            else :
                return jsonify({
                    "id": str(user["_id"]),
                    "email": user["email"],
                    "lastname": user["lastname"],
                    "firstname": user["firstname"],
                    "role": user["role"]
                })
        else:
            return jsonify({"error": "Utilisateur introuvable"}), 404
    else:
        return jsonify({"error": "Token invalide"}), 401

# Route pour récupérer tous les utilisateurs    
@app.route("/users", methods=["GET"])
def get_all_users():
    # Récupération de tous les utilisateurs
    all_users = users.find()
    user_list = []

    for user in all_users:
        user_data = {
            "id": str(user["_id"]),
            "email": user["email"],
            "lastname": user["lastname"],
            "firstname": user["firstname"],
            "role": user["role"]
        }
        user_list.append(user_data)

    if len(user_list) > 0:
        return jsonify(user_list)
    else:
        return jsonify({"error": "Aucun utilisateur trouvé"}), 404

# Route pour récupérer l'email de l'utilisateur
# @app.route("/email/<token>", methods=["GET"])
# def get_email(token):
#     user_id,status = token_manager.get_user_id_from_token(token)
#     if user_id:
#         user = users.find_one({"_id": user_id})
#         if user:
#             return jsonify({"email": user["email"]})
#         else:
#             return jsonify({"error": "Utilisateur introuvable"}), 404
#     else:
#         return jsonify({"error": "Token invalide"}), 401

# Route pour récupérer le nom de l'utilisateur
# @app.route("/lastname/<token>", methods=["GET"])
# def get_lastname(token):
#     user_id,status = token_manager.get_user_id_from_token(token)
#     if user_id:
#         user = users.find_one({"_id": user_id})
#         if user:
#             return jsonify({"nom": user["lastname"]})
#         else:
#             return jsonify({"error": "Utilisateur introuvable"}), 404
#     else:
#         return jsonify({"error": "Token invalide"}), 401

# Route pour récupérer le prénom de l'utilisateur
# @app.route("/firstname/<token>", methods=["GET"])
# def get_firstname(token):
#     user_id,status = token_manager.get_user_id_from_token(token)
#     if user_id:
#         user = users.find_one({"_id": user_id})
#         if user:
#             return jsonify({"prenom": user["firstname"]})
#         else:
#             return jsonify({"error": "Utilisateur introuvable"}), 404
#     else:
#         return jsonify({"error": "Token invalide"}), 401

# Route pour récupérer le role de l'utilisateur
# @app.route("/role/<token>", methods=["GET"])
# def get_role(token):
#     user_id,status = token_manager.get_user_id_from_token(token)
#     if user_id:
#         user = users.find_one({"_id": user_id})
#         if user:
#             return jsonify({"role": user["role"]})
#         else:
#             return jsonify({"error": "Utilisateur introuvable"}), 404
#     else:
#         return jsonify({"error": "Token invalide"}), 401


# Route pour modifier le mots de passe de l'utilisateur
# @app.route("/password/<token>", methods=["PUT"])
# def update_password(token):
#     user_id = token_manager.get_user_id_from_token(token)

#     if not user_id:
#         return jsonify({"error": "Token invalide"}), 401

#     data = request.get_json()
#     new_password = data["newMDP"]

#     # Hasher le nouveau mot de passe
#     hashed_password, salt = password_manager.hash_password(new_password)

#     # Mettre à jour le mot de passe dans la base de données
#     result = users.update_one({"_id": user_id}, {"$set": {"password": hashed_password, "salt": salt}})

#     # Vérifier si la mise à jour a réussi
#     if result.modified_count > 0:
#         return jsonify({"message": "Mot de passe mis à jour avec succès"}), 200
#     else:
#         return jsonify({"error": "Échec de la mise à jour du mot de passe"}), 500


# Route pour supprimer un utilisateur
@app.route("/user", methods=["DELETE"])
def delete_user():
    # Récupération du token
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return jsonify({"error": "Token non fourni"}), 403

    token_parts = auth_header.split(" ")
    if len(token_parts) != 2 or token_parts[0].lower() != "bearer":
        return jsonify({"error": "Schéma d'authentification non supporté"}), 401

    token = token_parts[1]
    
    admin_id,status = token_manager.get_user_id_from_token(token)
    data = request.get_json()
    user_id = data["id"]
    
    if not admin_id:
        return jsonify({"error": "Token invalide"}), 401

    admin = users.find_one({"_id": ObjectId(admin_id), "role": "admin"})
    if not admin:
        return jsonify({"error": "Accès non autorisé"}), 403

    user = users.find_one({"_id": ObjectId(user_id)})

    if not user:
        return jsonify({"error": "Utilisateur introuvable"}), 404

    users.delete_one({"_id": ObjectId(user_id)})
    return jsonify({"message": "Utilisateur supprimé avec succès"}), 200

# Route pour modifier le role de l'utilisateur
# @app.route("/role/<token>", methods=["PUT"])
# def update_user_role(token):
#     admin_id = token_manager.get_user_id_from_token(token)

#     if not admin_id:
#         return jsonify({"error": "Token invalide"}), 401

#     admin = users.find_one({"_id": admin_id, "role": "admin"})

#     if not admin:
#         return jsonify({"error": "Accès non autorisé"}), 403

#     data = request.get_json()
#     new_role = data["newRole"]
#     token_user_edit = data["token"]
#     user_id,status = token_manager.get_user_id_from_token(token_user_edit)
#     if user_id:
#         user = users.find_one({"_id": user_id})


#     if not user:
#         return jsonify({"error": "Utilisateur introuvable"}), 404


#     users.update_one({"_id": user_id}, {"$set": {"role": new_role}})

#     return jsonify({"message": "Rôle utilisateur mis à jour avec succès"}), 200

# Route pour modifier les champs que l'on désire pour un utilisateur donné
@app.route("/user", methods=["PATCH"])
def update_user():
    # Récupération du token
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        return jsonify({"error": "Token non fourni"}), 403

    token_parts = auth_header.split(" ")
    if len(token_parts) != 2 or token_parts[0].lower() != "bearer":
        return jsonify({"error": "Schéma d'authentification non supporté"}), 401

    token = token_parts[1]

    # Vérification de l'administrateur
    admin_id,status = token_manager.get_user_id_from_token(token)

    if not admin_id:
        return jsonify({"error": "Token invalide"}), 401

    admin = users.find_one({"_id": admin_id, "role": "admin"})

    if not admin:
        return jsonify({"error": "Accès non autorisé"}), 403


    #Vérification de l'utilisateur à modifier
    data = request.get_json()
    user_id = data["id"]
    user = users.find_one({"_id": ObjectId(user_id)})

    if not user:
        return jsonify({"error": "Utilisateur introuvable"}), 404

    # Récupération des données de modification
    allowed_fields = ["lastname", "firstname", "email", "password","role"]
    update_fields = {}

    for field in allowed_fields:
        if field in data:
            update_fields[field] = data[field]

    # Mise à jour des champs modifiés
    if update_fields:
        users.update_one({"_id":  ObjectId(user_id)}, {"$set": update_fields})
        user = users.find_one({"_id": ObjectId(user_id)})
        return jsonify({
            "id": str(user["_id"]),
                "email": user["email"],
                "lastname": user["lastname"],
                "firstname": user["firstname"],
                "role": user["role"]
            })
    


