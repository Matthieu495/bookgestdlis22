from utils.password import Password

password = Password()

def test_password_hashing():
    # Test avec un mot de passe simple
    hashed_password, salt = password.hash_password('password123')
    assert len(hashed_password) == 64
    assert len(salt) == 32

    # Test avec un mot de passe vide
    hashed_password, salt = password.hash_password('')
    assert len(hashed_password) == 64
    assert len(salt) == 32

    # Test avec un mot de passe contenant des caractères spéciaux
    hashed_password, salt = password.hash_password('P@ssw0rd!198')
    assert len(hashed_password) == 64
    assert len(salt) == 32

def test_password_verification():
    # Mot de passe correct
    hashed_password, salt = password.hash_password('fr14@NVqqq')
    assert password.verify_password('fr14@NVqqq', hashed_password, salt) is True

    # Mot de passe incorrect
    assert password.verify_password('xxxxxxxxxx', hashed_password, salt) is False

    # Mot de passe vide
    hashed_password, salt = password.hash_password('')
    assert password.verify_password('', hashed_password, salt) is True

    # Mot de passe contenant des caractères spéciaux
    hashed_password, salt = password.hash_password('P@ssw0rd!')
    assert password.verify_password('P@ssw0rd!', hashed_password, salt) is True
    assert password.verify_password('p@ssw0rd!', hashed_password, salt) is False


