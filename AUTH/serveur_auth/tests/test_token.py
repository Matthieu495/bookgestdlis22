from utils.token import Token
from utils.user import User
import jwt
import datetime
import jwt
from bson.objectid import ObjectId
import time


token_manager = Token()


def test_generate_token():
    
    # Création de deux utilisateurs
    user_1 = {
    "_id": "64886de3d79f3028916c2798",
    "lastname": "Dubois",
    "firstname": "Mike",
    "email": "mike@gmail.com",
    "password": "mike",
    "role": "admin"
    }

    user_2 = {
    "_id": "64886de3d79f3028916c21654",
    "lastname": "Dubois",
    "firstname": "Mike",
    "email": "mike@gmail.com",
    "password": "mike",
    "role": "admin"
    }
    
    # Cas de test : Vérifier que le jeton généré est une chaîne non vide
    token = token_manager.generate_token(user_1)
    assert isinstance(token, str)
    assert len(token) > 0

    # Cas de test : Vérifier que des jetons générés successivement sont différents (prise en compte de l'id)
    token1 = token_manager.generate_token(user_1)
    token2 = token_manager.generate_token(user_2)
    assert token1 != token2

    # Cas de test : Vérifier que des jetons générés à un intervalle de temps différents ne sont pas les memes (prise en compte de la date d'aujourd'hui)
    time.sleep(5)
    token3 = token_manager.generate_token(user_1)
    assert token1 != token3



def test_validate_token_valid():
    # Création d'un utilisateur
    user = {
    "_id": "64886de3d79f3028916c2798",
    "lastname": "Dubois",
    "firstname": "Mike",
    "email": "mike@gmail.com",
    "password": "mike",
    "role": "admin",
    "token":None
    }
    
    # On vérifie que le token est bien valide et que la méthode validate_token fonctionne
    token = token_manager.generate_token(user)
    user["token"] = token

    result = token_manager.validate_token(user, token)
    assert result is True

def test_validate_token_invalid():
    # Création d'un utilisateur
    user = {
    "_id": "64886de3d79f3028916c2798",
    "lastname": "Dubois",
    "firstname": "Mike",
    "email": "mike@gmail.com",
    "password": "mike",
    "role": "admin",
    "token":None
    }
    token = "xxxx"
    
    # On vérifie que le token non valide est rejetté pour savoir si la méthode validate_token fonctionne
    result = token_manager.validate_token(user, token)
    assert result is False

def test_get_payload_valid_token():
    # Création d'un utilisateur
    user = {
    "_id": "64886de3d79f3028916c2798",
    "lastname": "Dubois",
    "firstname": "Mike",
    "email": "mike@gmail.com",
    "password": "mike",
    "role": "admin",
    "token":None
    }

    # On vérifie que l'on obtient grace au token l'id de l'utilisateur (cas 1)
    token = token_manager.generate_token(user)
    payload, status = token_manager.get_payload(token)

    assert status == 'valid'
    assert payload is not None
    assert user["_id"] == payload["id"]

def test_get_payload_expired_token():
        # On vérifie que l'on obtient grace au token expiré (-1h) 'expired' (cas 2)
        expired_token = jwt.encode({'id': str(ObjectId()),'exp': datetime.datetime.utcnow() - datetime.timedelta(hours=1)}, 'secret_key', algorithm='HS256')
        payload, status = token_manager.get_payload(expired_token)

        assert status == 'expired'
        assert payload is None

def test_get_payload_invalid_token():
       # On vérifie que l'on obtient grace au token invalid 'invalid' (cas 3)
       invalid_token = "xxxxxx"
       payload, status = token_manager.get_payload(invalid_token)

       assert status == 'invalid'
       assert payload is None

def test_get_payload_none_token():
    # On vérifie que l'on obtient grace au token None invalid (cas 4)
    payload, status = token_manager.get_payload(None)

    assert status == 'invalid'
    assert payload is None

def test_get_user_id_from_token():
    # On vérifie que l'on extrait l'id via le payload  (cas 1)
    token = jwt.encode({'id': str(ObjectId()), 'exp': 10000000000}, 'secret_key', algorithm='HS256')

    result = token_manager.get_user_id_from_token(token)
    assert isinstance(result, tuple) and len(result) == 2

    user_id, status = result

    assert user_id is not None
    assert status == 'valid'

def test_get_user_id_from_expired_token():
    # On vérifie qu'on n'extrait pas l'id via le payload car le token est expiré (cas 2)
    token = jwt.encode({'id': str(ObjectId()), 'exp': 1}, 'secret_key', algorithm='HS256')
    token = token

    user_id, status = token_manager.get_user_id_from_token(token)

    assert user_id is None
    assert status == 'expired'

def test_get_user_id_from_invalid_token():
    # On vérifie qu'on n'extrait pas l'id via le payload car le token est invalide  (cas ) 
    token = 'xxxxxx'

    user_id, status = token_manager.get_user_id_from_token(token)

    assert user_id is None
    assert status == 'invalid'

