from app import app,users
from utils.token import Token

app.config["TESTING"] = True
client = app.test_client()

def test_register():
    # Données de l'utilisateur à enregistrer
    user = {
        "lastname": "Menfaim",
        "firstname": "Gérard",
        "email": "gerard.menfaim@gmail.com",
        "password": "cantine!!!SELF1",
        "password_confirmation": "cantine!!!SELF1",
    }

    # Envoie une requête POST à l'endpoint "/register" avec les données de l'utilisateur
    response = client.post("/register", json=user)
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 201

    # Vérifie le message de réussite de l'inscription
    assert "message" in data
    assert data["message"] == "Inscription réussie"

    # Vérifie si l'utilisateur existe dans la base de données
    assert users.find_one({"email": "gerard.menfaim@gmail.com"}) is not None



def test_register_existing_user():
    # Utilisateur déjà existant dans la base de données
    user = {
        "lastname": "Menfaim",
        "firstname": "Gérard",
        "email": "gerard.menfaim@gmail.com",
        "password": "cantine$$SELF2",
        "password_confirmation": "cantine$$SELF2",
    }

    # Envoie une requête POST à l'endpoint "/register" avec les données de l'utilisateur existant
    response = client.post("/register", json=user)
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 409

    # Vérifie le message d'erreur d'utilisateur existant
    assert "message" in data
    assert data["message"] == "Un utilisateur avec cet email existe déjà"

def test_register_failure():
    # Données incomplètes pour l'inscription
    user = {
        "lastname": "Menfaim",
        "firstname": "Gérard",
        # Pas d'email et de mots de passe
    }

    # Envoie une requête POST à l'endpoint "/register" avec des données incomplètes
    response = client.post("/register", json=user)
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 400

    # Vérifie le message d'échec de l'inscription
    assert "message" in data
    assert data["message"] == "Champs requis manquants : email, password, password_confirmation"

def test_register_failure_diff_pwd():
    # Données incomplètes pour l'inscription
    user = {
        "lastname": "Menfaim",
        "firstname": "Gérard",
        "email": "gerard.menfaim@gmail.com",
        "password": "cantine!!!SELF1",
        "password_confirmation": "cantine!!!SELF2",
    }

    # Envoie une requête POST à l'endpoint "/register" avec des données incomplètes
    response = client.post("/register", json=user)
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 400

    # Vérifie le message d'échec de l'inscription
    assert "message" in data
    assert data["message"] == "Les mots de passe sont différents"


def test_delete_user():
    # Token d'administrateur valide
    token_manager = Token()
    user = {
        '_id' : "64886de3d79f3028916c2798",
        'role' : 'admin'
    }
    admin_token = token_manager.generate_token(user)

    # id de l'utilisateur à supprimer
    id = users.find_one({"email": "gerard.menfaim@gmail.com"})

    # Envoie une requête DELETE à l'endpoint "/user" avec le token admin et l'id de l'utilisateur à supprimer
    headers = {
        'Authorization': f'Bearer {admin_token}'
    }
    response = client.delete("/user", headers=headers, json={'id': str(id['_id'])})
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 200

    # Vérifie le message de suppression réussie
    assert "message" in data
    assert data["message"] == "Utilisateur supprimé avec succès"
