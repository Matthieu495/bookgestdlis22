from app import app

app.config["TESTING"] = True
client = app.test_client()

def test_login_successful():
    # Test avec un utilisateur "Tintin Mystère" enregistré dans la base de données
    email = "tintin.mystere@example.com"
    password = "azerty"

    # Vérification que cela se passe bien avec les bons identifiants (cas 1)
    response = client.post("/login", json={"email": email, "password": password})
    data = response.get_json()

    assert response.status_code == 200
    assert "message" in data
    assert "token" in data

def test_login_incorrect_password():
    # Test avec un utilisateur "Tintin Mystère" enregistré dans la base de données
    email = "tintin.mystere@example.com"
    password = "xxx"

    # Vérification que le mot de passe est incorrect (cas 2)
    response = client.post("/login", json={"email": email, "password": password})
    data = response.get_json()

    assert response.status_code == 401
    assert "message" in data
    assert data["message"] == "Échec de la connexion : Mot de passe incorrect"

def test_login_incorrect_user():
    # Test avec un utilisateur "Tintin Mystère" enregistré dans la base de données
    email = "tintin@example.com"
    password = "xxx"

    # Vérification que l'email' est incorrect (cas 2)
    response = client.post("/login", json={"email": email, "password": password})
    data = response.get_json()

    assert response.status_code == 401
    assert "message" in data
    assert data["message"] == "Échec de la connexion : Utilisateur introuvable"
