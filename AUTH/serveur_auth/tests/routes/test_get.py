from app import app
from utils.token import Token

app.config["TESTING"] = True
client = app.test_client()

# Crée un utilisateur global pour les tests
user = {
    "_id" :"648ac5c37fc0c579f6c75df6",
    "email": "tintin.mystere@example.com",
    "lastname": "Mystère",
    "firstname": "Tintin",
    "password": "azerty",
    "token":None
}

token_manager = Token()

# def test_get_user_valid_token():
#     # Génère un token valide pour l'utilisateur global
#     token = token_manager.generate_token(user)
#     user["token"] = token
#     # Envoie une requête GET à l'endpoint "/user/<token>" avec le token valide
#     response = client.get("/user/"+token)
#     data = response.get_json()

#     # Vérifie le code de statut de la réponse
#     assert response.status_code == 200

#     # Vérifie les données renvoyées
#     assert "email" in data
#     assert data["email"] == user["email"]
#     assert "nom" in data
#     assert data["nom"] == user["lastname"]
#     assert "prenom" in data
#     assert data["prenom"] == user["firstname"]
#     assert "role" in data

# def test_get_user_invalid_token():
#     # Utilise un token invalide
#     token = "xxxx"

#     # Envoie une requête GET à l'endpoint "/user/<token>" avec le token invalide
#     response = client.get("/user/"+token)
#     data = response.get_json()

#     # Vérifie le code de statut de la réponse
#     assert response.status_code == 401

#     # Vérifie le message d'erreur spécifique
#     assert "error" in data
#     assert data["error"] == "Token invalide"

# def test_get_user_user_not_found():
#     # Génère un token valide pour l'utilisateur global
#     token = token_manager.generate_token(user)

#     # Modifie le token pour qu'il ne corresponde à aucun utilisateur
#     token += "abc123"

#     # Envoie une requête GET à l'endpoint "/user/<token>" avec le token modifié
#     response = client.get(f"/user/{token}")
#     data = response.get_json()

#     # Vérifie le code de statut de la réponse
#     assert response.status_code == 404

#     # Vérifie le message d'erreur spécifique
#     assert "error" in data
#     assert data["error"] == "Utilisateur introuvable"
