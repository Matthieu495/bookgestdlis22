from app import app,users
from utils.token import Token

app.config["TESTING"] = True
client = app.test_client()


def test_logout_successful():
    # Création d'un nouvel utilisateur
    user_data = {
        "email": "jean.neymar@psg.com",
        "lastname": "neymar",
        "firstname": "jean",
        "password": "allezlom@$OM13",
        "password_confirmation":"allezlom@$OM13"
    }
    response = client.post("/register", json=user_data)
    assert response.status_code == 201

    token = users.find_one({"email": "jean.neymar@psg.com"})["token"]

    # Déconnexion de l'utilisateur en utilisant le token valide
    headers = {
        'Authorization': f'Bearer {token}'
    }
    response = client.patch("/logout", headers=headers)
    data = response.get_json()

    # Vérification que l'utilisateur est bien déconnecté > sans token
    assert response.status_code == 200
    assert "message" in data
    assert data["message"] == "Déconnexion réussie"


def test_delete_user():
    # Token d'administrateur valide
    token_manager = Token()
    user = {
        '_id' : "64886de3d79f3028916c2798",
        'role' : 'admin'
    }
    admin_token = token_manager.generate_token(user)

    # id de l'utilisateur à supprimer
    id = users.find_one({"email": "jean.neymar@psg.com"})

    # Envoie une requête DELETE à l'endpoint "/user" avec le token admin et l'id de l'utilisateur à supprimer
    headers = {
        'Authorization': f'Bearer {admin_token}'
    }
    response = client.delete("/user", headers=headers, json={'id': str(id['_id'])})
    data = response.get_json()

    # Vérifie le code de statut de la réponse
    assert response.status_code == 200

    # Vérifie le message de suppression réussie
    assert "message" in data
    assert data["message"] == "Utilisateur supprimé avec succès"
