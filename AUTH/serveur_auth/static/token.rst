Token
=====

La classe `Token` fournit des méthodes pour la génération, la validation et l'extraction d'informations à partir de jetons JWT (JSON Web Token).

Méthodes
--------

- `generate_token(user)`
    Cette méthode génère un jeton JWT en utilisant les informations de l'utilisateur spécifié.

    :param user: Les informations de l'utilisateur.
    :type user: dict
    :return: Le jeton JWT généré.
    :rtype: str

- `validate_token(user, token)`
    Cette méthode valide un jeton JWT en comparant le jeton stocké dans les informations de l'utilisateur avec le jeton spécifié.

    :param user: Les informations de l'utilisateur.
    :type user: dict
    :param token: Le jeton JWT à valider.
    :type token: str
    :return: True si le jeton est valide, False sinon.
    :rtype: bool

- `get_payload(token)`
    Cette méthode retourne le contenu du payload d'un jeton JWT spécifié, ainsi que l'état de validité du jeton.

    :param token: Le jeton JWT.
    :type token: str
    :return: Le contenu du payload et l'état de validité du jeton.
    :rtype: tuple

- `get_user_id_from_token(token)`
    Cette méthode extrait l'ID de l'utilisateur à partir d'un jeton JWT spécifié, ainsi que l'état de validité du jeton.

    :param token: Le jeton JWT.
    :type token: str
    :return: L'ID de l'utilisateur et l'état de validité du jeton.
    :rtype: tuple

