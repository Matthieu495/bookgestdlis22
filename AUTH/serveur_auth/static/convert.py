import os
import glob
import docutils.core

rst_files = glob.glob("*.rst")

# Parcourir chaque fichier .rst
for rst_file in rst_files:
    html_file = os.path.splitext(rst_file)[0] + ".html"
    
    docutils.core.publish_file(
        source_path=rst_file,
        destination_path=html_file,
        writer_name="html"
    )
