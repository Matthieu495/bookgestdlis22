Password
========

La classe `Password` fournit des méthodes pour hacher et vérifier les mots de passe en utilisant une fonction de hachage.

Méthodes
--------

- `hash_password(password)`
    Cette méthode prend un mot de passe en tant que paramètre et renvoie le mot de passe haché et le sel utilisé pour le hachage.

    :param password: Le mot de passe à hacher.
    :type password: str
    :return: Un tuple contenant le mot de passe haché et le sel.
    :rtype: tuple

- `verify_password(password, hashed_password, salt)`
    Cette méthode vérifie si un mot de passe correspond à son hachage en utilisant le sel spécifié.

    :param password: Le mot de passe à vérifier.
    :type password: str
    :param hashed_password: Le mot de passe haché stocké.
    :type hashed_password: str
    :param salt: Le sel utilisé pour le hachage.
    :type salt: str
    :return: True si le mot de passe correspond, False sinon.
    :rtype: bool

