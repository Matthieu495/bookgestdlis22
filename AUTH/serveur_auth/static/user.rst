User
====

La classe `User` représente un utilisateur avec des informations telles que l'email, le nom, le prénom, le mot de passe, le sel, le rôle et le jeton.

Attributs
---------

- `email` (str) : L'adresse e-mail de l'utilisateur.
- `lastname` (str) : Le nom de famille de l'utilisateur.
- `firstname` (str) : Le prénom de l'utilisateur.
- `password` (str) : Le mot de passe de l'utilisateur (haché).
- `salt` (str) : Le sel utilisé pour hacher le mot de passe.
- `role` (str) : Le rôle de l'utilisateur.
- `token` (str) : Le jeton d'authentification de l'utilisateur.

Méthodes spéciales
------------------

- `__init__(email, lastname, firstname, password, salt, role, token)`
    Le constructeur de la classe `User` qui initialise les attributs de l'utilisateur avec les valeurs spécifiées.

    :param email: L'adresse e-mail de l'utilisateur.
    :type email: str
    :param lastname: Le nom de famille de l'utilisateur.
    :type lastname: str
    :param firstname: Le prénom de l'utilisateur.
    :type firstname: str
    :param password: Le mot de passe de l'utilisateur (haché).
    :type password: str
    :param salt: Le sel utilisé pour hacher le mot de passe.
    :type salt: str
    :param role: Le rôle de l'utilisateur.
    :type role: str
    :param token: Le jeton d'authentification de l'utilisateur.
    :type token: str
