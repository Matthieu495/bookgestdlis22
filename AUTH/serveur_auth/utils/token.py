import jwt
import datetime
from bson.objectid import ObjectId


class Token:
    def generate_token(self,user):
        # Logique de génération du jeton
        payload = {
            'id': str(user['_id']),
            'role': user['role'],
            'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=24)  # Expiration du jeton après 24 heures
        }
        token = jwt.encode(payload,'secret_key', algorithm='HS256')
        return token
    
    def validate_token(self,user,token):
        # Logique de validation du jeton
        if user["token"] == token:
            return True
        else:
            return False
        
    def get_payload(self,token):
        try:
            payload = jwt.decode(token,'secret_key', algorithms=['HS256'])
            return payload, 'valid'
        except jwt.ExpiredSignatureError:
            # Gérer les jetons expirés
            return None, 'expired'
        except jwt.InvalidTokenError:
            # Gérer les jetons invalides
            return None, 'invalid'
        
    def get_user_id_from_token(self,token):
        try:
            payload = jwt.decode(token,'secret_key', algorithms=['HS256'])
            return ObjectId(payload['id']),'valid'
        except jwt.ExpiredSignatureError:
            # Gérer les jetons expirés
            return None, 'expired'
        except jwt.InvalidTokenError:
            # Gérer les jetons invalides
            return None, 'invalid'
