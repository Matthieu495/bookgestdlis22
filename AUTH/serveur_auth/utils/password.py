import hashlib
import os

class Password:

    def hash_password(self,password):
        # Générer un sel aléatoire pour chaque mot de passe
        salt = os.urandom(16)
        
        # Concaténer le sel et le mot de passe
        salted_password = salt + password.encode('utf-8')
        
        # Utiliser une fonction de hachage (par exemple, SHA256) pour hacher le mot de passe
        hashed_password = hashlib.sha256(salted_password).hexdigest()
        
        # Retourner le mot de passe haché avec le sel
        return hashed_password, salt.hex()

    def verify_password(self,password, hashed_password, salt):

        # Convertir le sel en bytes
        salt = bytes.fromhex(salt)
        
        # Concaténer le sel et le mot de passe
        salted_password = salt + password.encode('utf-8')
        
        # Hacher le mot de passe avec le sel
        hashed_input = hashlib.sha256(salted_password).hexdigest()
        
        # Vérifier si le mot de passe haché correspond au mot de passe haché stocké
        if hashed_input == hashed_password:
            return True
        
        return False
